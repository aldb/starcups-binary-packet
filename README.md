# CUPS Packet

Debian package that installs packages needed by TSP printer.
To be used if problems occur during installation with the original 3.5.0 driver provided by Star

## Updating
Edit the starcups-packet/DEBIAN/control file for dependencies and version.
Add or remove files in starcups-packet directory.

## Creating the package

Run in terminal:
make
